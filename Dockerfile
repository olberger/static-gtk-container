FROM debian:stretch

ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_PRIORITY critical
ENV DEBCONF_NOWARNINGS yes

#
# Update apt
#
RUN apt-get update -q -q
RUN apt-get upgrade --yes --force-yes

# Build dependencies of gtk-3.0 (see for instance https://tracker.debian.org/media/packages/g/gtk%2B3.0/control-3.22.11-1)
RUN apt-get -y install \
    debhelper cdbs gnome-pkg-tools dpkg-dev gtk-doc-tools  dh-autoreconf docbook-xml docbook-xsl pkg-config autotools-dev dbus  gsettings-desktop-schemas adwaita-icon-theme  at-spi2-core  libglib2.0-dev  libgdk-pixbuf2.0-dev  libpango1.0-dev  libatk1.0-dev  libatk-bridge2.0-dev libegl1-mesa-dev  libepoxy-dev libfontconfig1-dev libharfbuzz-dev  libwayland-dev   wayland-protocols   libxkbcommon-dev  libx11-dev libxext-dev libxi-dev libxml2-utils libxrandr-dev  libxcursor-dev libxcomposite-dev libxdamage-dev libxkbfile-dev libxinerama-dev libxfixes-dev libcairo2-dev  libcups2-dev  libcolord-dev  librest-dev libjson-glib-dev gobject-introspection  libgirepository1.0-dev  xauth  xsltproc xvfb

WORKDIR /gtk

# Grabbed from cdn-fastly.deb.debian.org/debian/pool/main/g/gtk+3.0/gtk+3.0_3.22.11.orig.tar.xz for instance
COPY gtk+3.0_3.22.11.orig.tar.xz /gtk/

RUN tar xf gtk+3.0_3.22.11.orig.tar.xz

# This will be done at run-time instead
#RUN mkdir /gtk/result

RUN cd gtk+-3.22.11/ ; \
    ./configure --enable-static --disable-modules --enable-broadway-backend --disable-x11-backend --disable-wayland-backend --prefix=/result

# At run time, we'll set SRC_SUBDIRS to override compilation of tests which fail with broadway (?)
# cd gtk+-3.22.11/ ; \
#    make SRC_SUBDIRS="gdk gtk libgail-util modules demos examples" install
    
