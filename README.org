#+TITLE: Gtk 3.0 static compilation in a Debian stable container

* Purpose

This is an attempt at building static libraries of Gtk in a clean room
(using a Debian base image) to allow static compilation of Gtk/Gdk
programs, with the broadway Gdk backend.

Broadway allows the display of Gtk interface in a HTML5 canvas.

The end goal could be to run Gtk apps over WebAssemble (aka wasm)

* Build docker container image
See [[file:build.sh]] which relies on [[file:Dockerfile]]

* Run the container to build Gtk

See [[file:run.sh]]

The build is done by the standard toolchain with GCC, but ultimately
emscripten could be needed.


