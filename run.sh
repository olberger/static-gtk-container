#!/bin/sh

docker run -t -i -v `pwd`/result:/result olberger/debian-gtk-static:stretch /bin/sh -c 'cd /gtk/gtk+-3.22.11/ ;  make -j 6 SRC_SUBDIRS="gdk gtk libgail-util modules demos examples" install'

PKG_CONFIG_PATH=`pwd`/result/lib/pkgconfig pkg-config --cflags gtk+-3.0

 
